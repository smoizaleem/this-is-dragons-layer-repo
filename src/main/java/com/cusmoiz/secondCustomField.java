package com.cusmoiz;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.issue.customfields.impl.MultiSelectCFType;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.customfields.manager.OptionsManager;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.jira.issue.fields.config.manager.FieldConfigSchemeManager;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;

import java.util.List;
import java.util.Map;

public class secondCustomField extends MultiSelectCFType {
    private static final Logger log = LoggerFactory.getLogger(secondCustomField.class);
private static JiraBaseUrls jiraBaseUrls;
private static FeatureManager featureManager;
private static SearchService searchService=ComponentAccessor.getComponent(SearchService.class);
    public secondCustomField(OptionsManager optionsManager, CustomFieldValuePersister customFieldValuePersister, GenericConfigManager genericConfigManager) {
    super(optionsManager, customFieldValuePersister, genericConfigManager, jiraBaseUrls, searchService, featureManager);
}    
    @Override
    public Map<String, Object> getVelocityParameters(final Issue issue,
                                                     final CustomField field,
                                                     final FieldLayoutItem fieldLayoutItem) {
        final Map<String, Object> map = super.getVelocityParameters(issue, field, fieldLayoutItem);

        // This method is also called to get the default value, in
        // which case issue is null so we can't use it to add currencyLocale
        if (issue == null) {
            return map;
        }

         FieldConfig fieldConfig = field.getRelevantConfig(issue);
         //add what you need to the map here

         FieldConfigSchemeManager fieldConfigSchemeManager =
        		 ComponentAccessor.getComponent(FieldConfigSchemeManager.class);
        		 List<FieldConfigScheme> schemes = fieldConfigSchemeManager.getConfigSchemesForField(customField);
        return map;
        
    }
}