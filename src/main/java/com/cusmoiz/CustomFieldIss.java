package com.cusmoiz;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.atlassian.jira.issue.customfields.impl.AbstractMultiCFType;
import com.atlassian.jira.issue.customfields.converters.StringConverter;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.customfields.persistence.PersistenceFieldType;
import com.atlassian.jira.issue.customfields.view.CustomFieldParams;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.util.ErrorCollection;

import java.util.Comparator;
import java.util.List;
import java.util.Map;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class CustomFieldIss extends AbstractMultiCFType {
    private static final Logger log = LoggerFactory.getLogger(CustomFieldIss.class);

    public CustomFieldIss(CustomFieldValuePersister customFieldValuePersister, StringConverter stringConverter, GenericConfigManager genericConfigManager) {
    super(customFieldValuePersister, genericConfigManager);
}
    
    @Override
    public Map<String, Object> getVelocityParameters(final Issue issue,
                                                     final CustomField field,
                                                     final FieldLayoutItem fieldLayoutItem) {
        final Map<String, Object> map = super.getVelocityParameters(issue, field, fieldLayoutItem);

        // This method is also called to get the default value, in
        // which case issue is null so we can't use it to add currencyLocale
        if (issue == null) {
            return map;
        }

         FieldConfig fieldConfig = field.getRelevantConfig(issue);
         //add what you need to the map here

        return map;
    }

	public void createValue(CustomField arg0, Issue arg1, Object arg2) {
		// TODO Auto-generated method stub
		
	}

	@Nullable
	public String getChangelogValue(CustomField arg0, Object arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getSingularObjectFromString(String arg0)
			throws FieldValidationException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getStringFromSingularObject(Object arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getStringValueFromCustomFieldParams(CustomFieldParams arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getValueFromCustomFieldParams(CustomFieldParams arg0)
			throws FieldValidationException {
		// TODO Auto-generated method stub
		return null;
	}

	public void setDefaultValue(FieldConfig arg0, Object arg1) {
		// TODO Auto-generated method stub
		
	}

	public void updateValue(CustomField arg0, Issue arg1, Object arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void validateFromParams(CustomFieldParams arg0,
			ErrorCollection arg1, FieldConfig arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	@Nullable
	protected Object convertDbValueToType(@Nullable Object arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Nullable
	protected Object convertTypeToDbValue(@Nullable Object arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Nonnull
	protected PersistenceFieldType getDatabaseType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Nullable
	protected Comparator getTypeComparator() {
		// TODO Auto-generated method stub
		return null;
	}
}