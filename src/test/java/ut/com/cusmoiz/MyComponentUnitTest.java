package ut.com.cusmoiz;

import org.junit.Test;
import com.cusmoiz.MyPluginComponent;
import com.cusmoiz.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}